package org.abbtech.practice.lesson2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;


import java.util.List;

@Repository
public class TaskRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Task> findAll() {
        return jdbcTemplate.query("SELECT * FROM tasks", (rs, rowNum) ->
                new Task(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getString("description")
                ));
    }

    public Task findById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM tasks WHERE id = ?", new Object[]{id}, (rs, rowNum) ->
                new Task(
                        rs.getLong("id"),
                        rs.getString("title"),
                        rs.getString("description")
                ));
    }

    public Task save(Task task) {
        jdbcTemplate.update("INSERT INTO tasks (title, description) VALUES (?, ?)", task.getTitle(), task.getDescription());
        return task;
    }

    public Task update(Task task) {
        jdbcTemplate.update("UPDATE tasks SET title = ?, description = ? WHERE id = ?", task.getTitle(), task.getDescription(), task.getId());
        return task;
    }

    public void deleteById(Long id) {
        jdbcTemplate.update("DELETE FROM tasks WHERE id = ?", id);
    }
}
