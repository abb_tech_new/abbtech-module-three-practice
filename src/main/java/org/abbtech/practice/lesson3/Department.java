package org.abbtech.practice.lesson3;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table( name = "department")
@Data
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long departmentId;
    @Column(name = "departmentName")
    private String departmentName;

    @OneToMany(mappedBy = "department", cascade = CascadeType.REFRESH)
    private List<Professor> professors;


}
