package org.abbtech.practice.lesson3;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "professor")
@Data
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "professorid")
    private Long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "departmentid", referencedColumnName = "departmentid")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "bookid", referencedColumnName = "bookid")
    private Book book;

}
