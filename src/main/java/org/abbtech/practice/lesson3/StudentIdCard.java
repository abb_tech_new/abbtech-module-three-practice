package org.abbtech.practice.lesson3;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Date;

@Entity
@Table("student_id_card")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentIdCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cardId;
    @Column("card_number")
    private String cardNumber;
    @Column("expiry_date")
    private Date expiryDate;


}
