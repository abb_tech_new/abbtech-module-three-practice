package org.abbtech.practice.lesson3;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@Entity
@Table(name = "Student")
@Data
@NoArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long studentId;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;

    @OneToOne
    @JoinColumn(name = "cardID")
    private StudentIdCard studentIdCard;

    @ManyToMany(mappedBy = "student")
    private List<Class> studentClasses;

}
