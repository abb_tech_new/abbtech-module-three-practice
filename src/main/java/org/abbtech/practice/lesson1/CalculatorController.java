package org.abbtech.practice.lesson1;

import org.abbtech.practice.lesson1.calculatorAll.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/calculator")
public class CalculatorController {

    private final ApplicationService applicationService;

    public CalculatorController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping("/multiply")
    public int multiply(@RequestParam int a, @RequestParam int b) {
        return applicationService.multiply(a, b);
    }

    @GetMapping("/subtract")
    public int subtract(@RequestParam int a, @RequestParam int b) {
        return applicationService.subtract(a, b);
    }

    @GetMapping("/addition")
    public int addition(@RequestParam int a, @RequestParam int b) {
        return applicationService.addition(a, b);
    }

    @GetMapping("/division")
    public int division(@RequestParam int a, @RequestParam int b) {
        return applicationService.devision(a, b);
    }
}
