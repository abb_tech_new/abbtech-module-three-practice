package org.abbtech.practice.lesson1.calculatorAll;

import org.abbtech.practice.lesson1.calculatorAll.CalculatorService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServices implements CalculatorService {
    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

    @Override
    public int addition(int a, int b) {
        return a + b;
    }

    @Override
    public int devision(int a, int b) {
        return a / b;
    }
}
