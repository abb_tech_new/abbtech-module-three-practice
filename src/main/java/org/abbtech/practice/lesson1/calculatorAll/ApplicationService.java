package org.abbtech.practice.lesson1.calculatorAll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationService {
    private final CalculatorService calculatorService;

    @Autowired
    public ApplicationService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }


    public int multiply(int a, int b) {
        if (a > 4 && b > 6) {
            throw new ArithmeticException(" error ");
        }
        return calculatorService.multiply(a, b);
    }

    public int subtract(int a, int b) {
        var result = calculatorService.subtract(a, b);
        if (result == 2 || result == 4 || result == 6 || result == 8) {
            throw new ArithmeticException("error");
        }
        return result;
    }

    public int devision(int a, int b) {
        if (a % 2 == 0 && b % 2 == 0) {
            return calculatorService.devision(a, b);
        } else {
            throw new ArithmeticException(" error ");
        }
    }

    public int addition(int a, int b) {
        if (a >= 0 && b >= 0) {
            return calculatorService.addition(a, b);
        } else {
            throw new ArithmeticException(" error ");
        }
    }
}
